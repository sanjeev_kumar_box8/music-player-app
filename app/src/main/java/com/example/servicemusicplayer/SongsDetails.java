package com.example.servicemusicplayer;

import android.graphics.drawable.Drawable;

public class SongsDetails {

    private String name, path, album, artist, dispName, albumArt;

    private long id, albumId;
    private Drawable albumArtImg;

    public SongsDetails() {
        name = "";
        path = "";
        album = "";
        artist = "";
        dispName = "";
        id = 0;
    }

    public SongsDetails(String name, String path, String album, String artist, long id, String dispName, long albumId, String albumArt) {
        this.name = name;
        this.path = path;
        this.album = album;
        this.artist = artist;
        this.id = id;
        this.dispName = dispName;
        this.albumId = albumId;
        this.albumArt = albumArt;
        this.albumArtImg = Drawable.createFromPath(albumArt);
    }

    public Drawable getAlbumArtImg() {
        return albumArtImg;
    }

    public String getAlbumArt() {
        return albumArt;
    }

    public void setAlbumArt(String albumArt) {
        this.albumArt = albumArt;
    }

    public String getDispName() {
        return dispName;
    }

    public void setDispName(String dispName) {
        this.dispName = dispName;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }
}
