package com.example.servicemusicplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterActivityCommunicator, ServiceAdapterCommunicator, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private int STORAGE_PERMISSION_CODE = 1;
    private ArrayList<SongsDetails> songs = new ArrayList<>();
    private RecyclerView songListRecycler;
    private SongsAdapter songsAdapter;

    SongsService songsService;
    boolean songBound = false;

    ImageButton playPauseBtn;
    ImageButton nextBtn;
    ImageButton prevBtn;
    ImageButton globalPlayBtn;
    ImageButton globalPauseBtn;
    SeekBar seekBar;
    TextView songProgressText;
    TextView songDurationText;

    private SongsDetails selectedSong;
    private SongsDetails currSong; // the song which is playing now...
    private boolean isPaused;

    private SongsAdapter.SongsViewHolder songsViewHolder;
    private int selectedSongIndex;
    private int currSongIndex = -1;

    private Runnable runnable;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selectedSong = currSong = null;

        requestStoragePermission();

        playPauseBtn = findViewById(R.id.playPauseBtn);
        nextBtn = findViewById(R.id.nextBtn);
        prevBtn = findViewById(R.id.prevBtn);
        globalPlayBtn = findViewById(R.id.globalPlayBtn);
        globalPauseBtn = findViewById(R.id.globalPauseBtn);
        seekBar = findViewById(R.id.seekbar);
        songProgressText = findViewById(R.id.songProgressText);
        songDurationText = findViewById(R.id.songDurationText);

        handler = new Handler();

        playPauseBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        prevBtn.setOnClickListener(this);
        globalPlayBtn.setOnClickListener(this);
        globalPauseBtn.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);

        // collect songs from service...

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent serviceIntent = new Intent(this, SongsService.class);

        bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);
        startService(serviceIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        songBound = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            SongsService.SongBinder songBinder = (SongsService.SongBinder) service;
            songsService = songBinder.getService(MainActivity.this);
            songBound = true;

            setRecyclerView();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            songBound = false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("service", "Activity onResume()");
    }

    private void setRecyclerView() {

        songListRecycler = findViewById(R.id.songsRecycle);
        songListRecycler.setLayoutManager(new LinearLayoutManager(this));

        if (songBound) {
            songs = songsService.getSongs();
        }

        songsAdapter = new SongsAdapter(songs, this);
        songListRecycler.setAdapter(songsAdapter);
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setTitle("Permission Needed")
                    .setMessage("This is because it is needed")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeToast("Permission Granted");
            } else {
                makeToast("Permission Denied");
            }
        }
    }

    private void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void log(String text) {
        Log.i("songsList", text);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.playPauseBtn:
                playASongLogic();
                break;

            case R.id.nextBtn:

                songsService.playNextSong(currSongIndex);
                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                currSongIndex = (currSongIndex + 1) % songsAdapter.getItemCount();
                currSong = songs.get(currSongIndex);

//                if(currSong.equals(selectedSong)) {
//                    if(isPaused) {
//                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
//                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
//                    } else {
//                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
//                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
//                    }
//                }

                isPaused = false;
                if(currSong.equals(selectedSong)) {
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
                } else {
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                }

                break;

            case R.id.prevBtn:

                songsService.playPrevSong(currSongIndex);
                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                if (currSongIndex == 0) {
                    currSongIndex = songsAdapter.getItemCount() - 1;
                } else {
                    currSongIndex = (currSongIndex - 1) % songsAdapter.getItemCount();
                }
                currSong = songs.get(currSongIndex);

//                if(currSong.equals(selectedSong)) {
//                    if(isPaused) {
//                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
//                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
//                    } else {
//                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
//                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
//                    }
//                }

                isPaused = false;
                if(currSong.equals(selectedSong)) {
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
                } else {
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                }

                break;

            case R.id.globalPauseBtn:
                songsService.pauseCurrentSong();
                isPaused = !isPaused;
                if(currSong.equals(selectedSong)) {
                    if(isPaused) {
                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                    } else {
                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
                    }
                } else {
                    if(isPaused) {
                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                    } else {
                        globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                    }
                }

                break;
        }
    }

    @Override
    public void communicate(SongsDetails songsDetails, SongsAdapter.SongsViewHolder songsViewHolder, int index) {
        this.selectedSong = songsDetails;
        this.selectedSongIndex = index;
        this.songsViewHolder = songsViewHolder;
        if (!selectedSong.equals(currSong)) {
            playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
        }
        if (selectedSong.equals(currSong) && !isPaused) {
            playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
        }
        if (selectedSong.equals(currSong) && isPaused) {
            playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
        }
    }

    private void playASongLogic() {

        if (selectedSong == null) {
            makeToast("Tap on a song then tap on play button to listen to the song");
            return;
        }
        // we have selectedSong and currentSong...
        if (songBound) {
            //case 1: no music is playing... => currSong == null: play btn is showing.. make it pause btn
            if (currSong == null) {
                songsService.playSong(selectedSong);
                currSong = selectedSong;
                isPaused = false;
                currSongIndex = selectedSongIndex;
                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));

                globalPauseBtn.setVisibility(View.VISIBLE);
                globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
            } else if (currSong.equals(selectedSong) && !isPaused) { // case 2: currSong is playing and selected song = currSong...pause btn, meke it play btn
                songsService.pauseCurrentSong(); // pause
                isPaused = true;
                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));

                globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
            } else if (currSong.equals(selectedSong) && isPaused) {
                songsService.pauseCurrentSong(); // resume song (so make btn pause)
                isPaused = false;
                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));

                globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
            } else { // case 3: currSong is playing and selectedSong != currSong...set btn to play (in adapterActivityCommunicator), make it pause btn
                songsService.stopCurrentSong();
                songsService.playSong(selectedSong);
                currSong = selectedSong;
                currSongIndex = selectedSongIndex;
//                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
                isPaused = false;
                if(isPaused) {
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
                } else {
                    globalPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
                }
            }
//            if (!isPaused) {
//                globalPlayBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_pause_btn));
//            } else {
//                globalPlayBtn.setImageDrawable(getResources().getDrawable(R.drawable.global_play_btn));
//            }

        } else {
            makeToast("something went wrong... try again");
        }
    }

    @Override
    public void adapterGetsDataFromService(long sondId) {
        songsAdapter.setIndicator(sondId);
        songsAdapter.notifyDataSetChanged();
    }

    @Override
    public void setSeekbarDuration() {
        int duration = songsService.getSongDuration();
        seekBar.setMax(duration);
        duration /= 1000;
        int min = duration / 60;
        int sec = duration % 60;
        songDurationText.setText("" + min + ":" + sec);
    }

    @Override
    public void changeSeekbar() {

        int songProgress = songsService.getSongCurrentPosition();
        seekBar.setProgress(songProgress);
        songProgress /= 1000;
        int min = songProgress / 60;
        int sec = songProgress % 60;
        if(sec < 10) {
            songProgressText.setText("" + min + ":0" + sec);
        } else {
            songProgressText.setText("" + min + ":" + sec);
        }

        if (songsService.songIsPlaying()) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    changeSeekbar();
                }
            };
            handler.postDelayed(runnable, 100);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser)
            songsService.seekSongTo(progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}

//    @Override
//    public void communicate(SongsDetails songsDetails) {
//        Log.i("songsComm", songsDetails.getName() + " " + songsDetails.getPath());
//
//        if(selectedSong == null) {
//            selectedSong = songsDetails;
//        } else {
//            if(songsDetails.equals(currSong)) {
//                selectedSong = songsDetails;
//                Log.i("songsComm", "Same song selected");
//                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
//            } else {
//                selectedSong = songsDetails;
//                Log.i("songsComm", "Different song selected");
////                playPauseBtn.setText("play");
//                playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
//            }
//        }
//    }

//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.playPauseBtn:
////                if(playPauseBtn.getText().toString().equals("play")) {
//                if(playPauseBtn.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.playbtn).getConstantState())) {
//                    if (selectedSong == null) {
//                        makeToast("Tap on a song then tap on play button to listen to the song");
//                    } else {
//                        if (songBound) {
//                            songsService.stopCurrentSong();
//                            songsService.playSong(selectedSong);
//                            makeToast("Playing " + selectedSong.getName());
//                            currSong = selectedSong;
//                        } else {
//                            makeToast("try agian");
//                        }
////                        playPauseBtn.setImageDrawable("Pause");
//                        playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.pausebtn));
//                    }
//                } else {
//                    songsService.pauseCurrentSong();
////                    playPauseBtn.setText("play");
//                    playPauseBtn.setImageDrawable(getResources().getDrawable(R.drawable.playbtn));
//                }
//        }
//    }
//}