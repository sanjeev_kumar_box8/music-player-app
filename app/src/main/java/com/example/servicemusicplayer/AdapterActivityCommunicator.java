package com.example.servicemusicplayer;

public interface AdapterActivityCommunicator {
    void communicate(SongsDetails songsDetails, SongsAdapter.SongsViewHolder songsViewHolder, int index);
}
