package com.example.servicemusicplayer;

public interface ServiceAdapterCommunicator {
    void adapterGetsDataFromService(long index);

    void setSeekbarDuration();

    void changeSeekbar();
}
// SongService will send data to MainActivity.... MainActivity will call a method in SongAdapter to set indicator...