package com.example.servicemusicplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.servicemusicplayer.SongNotification.CHANNEL_ID;

public class SongsService extends Service {

    MediaPlayer mediaPlayer = new MediaPlayer();

    private ArrayList<SongsDetails> songs = new ArrayList<>();
    Uri songsUri;

    private final IBinder binder = new SongBinder();

    private ServiceAdapterCommunicator serviceAdapterCommunicator;

    public class SongBinder extends Binder {

        SongsService getService(ServiceAdapterCommunicator sac) {
            serviceAdapterCommunicator = sac;
            return SongsService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public ArrayList<SongsDetails> getSongs() {
        return songs;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("service", "onCreate");

        songs = findSongs(); // collect the songs here...

        if (songs == null) {
            makeToast("null is coming");
            return;
        }
        for (int i = 0; i < songs.size(); i++) {
            log(songs.get(i).getName());
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("service", "startCommand service started");
//        return super.onStartCommand(intent, flags, startId);

//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("ServiceMusicPlayer")
                .setContentText("get song details here")
                .setSmallIcon(R.drawable.notification_icon)
//                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
    }

    private void makeToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void log(String text) {
        Log.i("songsList", text);
    }

    public ArrayList<SongsDetails> findSongs() {

        ArrayList<SongsDetails> tempArrayList = new ArrayList<>();

        ContentResolver songContentResolver = getContentResolver();
        songsUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor songCursor = songContentResolver.query(songsUri, null, null, null, null);

        if (songCursor != null && songCursor.moveToFirst()) {
            Log.i("songCursor", "it's not empty");
            int titleIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int idIndex = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int artistIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int albumIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            int pathIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            int dispName = songCursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);
            int durationIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            int albumIdIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
//            int albumArtIndex = songCursor.getColumnIndex(MediaStore.Audio.AlbumColumns.ALBUM_ART);

            do {
                long id = songCursor.getLong(idIndex);
                String title = songCursor.getString(titleIndex);
                String artist = songCursor.getString(artistIndex);
                String album = songCursor.getString(albumIndex);
                String path = songCursor.getString(pathIndex);
                String name = songCursor.getString(dispName);
                String duration = songCursor.getString(durationIndex);
                long albumId = songCursor.getLong(albumIdIndex);
                String albumArt = new String();

                Cursor cursor = getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                        MediaStore.Audio.Albums._ID + "=?",
                        new String[]{String.valueOf(albumId)},
                        null);

                if (cursor != null && cursor.moveToFirst()) {
                    albumArt = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                    cursor.close();
                }

                tempArrayList.add(new SongsDetails(title, path, artist, album, id, name, albumId, albumArt));
                Log.i("songCursor", "added " + title + " " + artist + " " + duration);
            } while (songCursor.moveToNext());
        } else {
            Log.i("songCursor", "Did not find music files");
        }
        return tempArrayList;
    }

    public void playSong(SongsDetails songsDetails) {
        Uri songUri = Uri.parse("file://" + songsDetails.getPath());
        mediaPlayer = new MediaPlayer();

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(SongsService.this, songUri);
            mediaPlayer.prepare();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        serviceAdapterCommunicator.setSeekbarDuration();

        mediaPlayer.start();

        serviceAdapterCommunicator.changeSeekbar();
        makeToast("playing " + songsDetails.getName());

        serviceAdapterCommunicator.adapterGetsDataFromService(songsDetails.getId());
    }

    public void stopCurrentSong() {
        if (mediaPlayer != null)
            mediaPlayer.stop();
    }

    public void pauseCurrentSong() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            }
            else {
                mediaPlayer.start();
                serviceAdapterCommunicator.changeSeekbar();
            }
        }
    }

    public void playNextSong(int songIndex) {
        int nextSongIndex = (songIndex + 1) % songs.size();
        stopCurrentSong();
        playSong(songs.get(nextSongIndex));
    }

    public void playPrevSong(int songIndex) {
        int prevSongIndex;
        if(songIndex == 0) {
            prevSongIndex = songs.size() - 1;
        } else {
            prevSongIndex = (songIndex - 1) % songs.size();
        }
        stopCurrentSong();
        playSong(songs.get(prevSongIndex));
    }

    public void seekSongTo(int time) {
        mediaPlayer.seekTo(time);
    }

    public int getSongDuration() {
        return mediaPlayer.getDuration();
    }

    public int getSongCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public boolean songIsPlaying() {
        return mediaPlayer.isPlaying();
    }
}

//private void getAllSongs() throws IOException {
//    ContentResolver musicResolver = (SongService.this).getContentResolver();
//    Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//    Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);
//
//    if (musicCursor != null && musicCursor.moveToFirst()) {
//        //get columns
//        int titleColumn = musicCursor.getColumnIndex
//                (android.provider.MediaStore.Audio.Media.TITLE);
//        int idColumn = musicCursor.getColumnIndex
//                (android.provider.MediaStore.Audio.Media._ID);
//        int artistColumn = musicCursor.getColumnIndex
//                (android.provider.MediaStore.Audio.Media.ARTIST);
//        int albumIdColumn = musicCursor.getColumnIndex
//                (MediaStore.Audio.Media.ALBUM_ID);
//        int albumKeyColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_KEY);
//        int data= musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
//
//        Log.d("123-Song Count", String.valueOf(musicCursor.getCount()));
//        //add songs to list
//        do {
//            long iD = musicCursor.getLong(idColumn);
//            String title = musicCursor.getString(titleColumn);
//            String artist = musicCursor.getString(artistColumn);
//            long albumId = musicCursor.getLong(albumIdColumn);
//            String albumKey = musicCursor.getString(albumKeyColumn);
//            String thisData= musicCursor.getString(data);
//            String artPath = "";
//
//
//            Cursor cursor = getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
//                    new String[]{MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
//                    MediaStore.Audio.Albums._ID + "=?",
//                    new String[]{String.valueOf(albumId)},
//                    null);
//
//            if (cursor != null && cursor.moveToFirst()) {
//                artPath = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
//                cursor.close();
//            }
//
//            tempSongList.add(new Song(iD, title, artist, albumId, albumKey, artPath, thisData));
//
//        }
//        while (musicCursor.moveToNext());
//
//        musicCursor.close();
//    }
//}

//    public ArrayList<SongsDetails> findSongs(final Context context) {
//
//        ArrayList<SongsDetails> songsList = new ArrayList<>();
//
//        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//        String[] projection = new String[]{MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST,};
//        Cursor cursor = context.getContentResolver().query(uri, projection, MediaStore.Audio.Media.DATA + " like ? ", new String[]{"%utm"}, null);
////        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
//        if (cursor != null) {
//            while (cursor.moveToNext()) {
//
//                String path = cursor.getString(0);
//                String name = cursor.getString(1);
//                String album = cursor.getString(2);
//                String artist = cursor.getString(3);
//
//                songsList.add(new SongsDetails(name, path, album, artist));
//
//                Log.i("songsListProvider", name + " " + path + " " + album + " " + artist);
//            }
//        } else {
//            Log.i("songsListProvider", "Provider method failed");
//        }
//
//        return songsList;
//    }


//    ArrayList<File> findSongs(File root) {
//
//        ArrayList<File> fileList = new ArrayList<>();
//        File[] files = root.listFiles();
//
//        if (files == null) {
//            Log.i("nullIsComing", "returned null");
//            return null;
//        } else {
//            Log.i("nullIsComing", "not returned null");
//        }
//
//        for (File singleFile : files) {
//            if (singleFile.isDirectory() && !singleFile.isHidden()) {
//                fileList.addAll(findSongs(singleFile));
//            } else {
//                if (singleFile.getName().endsWith(".mp3") || singleFile.getName().endsWith(".wav")) {
//                    fileList.add(singleFile);
//                    Log.i("nullIsComing", singleFile.getName());
//                }
//            }
//        }
//        return fileList;
//    }
