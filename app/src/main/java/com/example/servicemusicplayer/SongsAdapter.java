package com.example.servicemusicplayer;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class SongsAdapter extends RecyclerView.Adapter<SongsAdapter.SongsViewHolder> {

    //    private ArrayList<File> data;
    private int songIndex = -1;
    private ArrayList<SongsDetails> data;

    AdapterActivityCommunicator adapterActivityCommunicator;
    long currSongId;

    //    public SongsAdapter(ArrayList<File> data) {
    public SongsAdapter(ArrayList<SongsDetails> data, AdapterActivityCommunicator listener) {
        this.data = data;
        this.adapterActivityCommunicator = listener;
    }

    @NonNull
    @Override
    public SongsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.songs_list_layout, parent, false);
        return new SongsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SongsViewHolder holder, final int position) {

        final String songName = data.get(position).getDispName();
        holder.songsText.setText(songName);
        holder.songsArtist.setText(data.get(position).getArtist());
        if(data.get(position).getAlbumArt() != null) {
            holder.songsArt.setImageDrawable(data.get(position).getAlbumArtImg());
        }

        holder.songsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("songsList", "you tapped on " + holder.songsArtist.getText() + " " + position);
                songIndex = position;
                notifyDataSetChanged();
                adapterActivityCommunicator.communicate(data.get(position), holder, position);
            }
        });

        if(songIndex == position) {
            holder.songsText.setBackgroundResource(R.color.selectedSongBackground);
            holder.songsArtist.setBackgroundResource(R.color.selectedSongBackground);
        } else {
            holder.songsText.setBackgroundResource(R.color.defaultSongBackground);
            holder.songsArtist.setBackgroundResource(R.color.defaultSongBackground);
        }

        if(data.get(position).getId() == currSongId) {
            holder.songsText.setBackgroundResource(R.color.playingSongBackground);
            holder.currentlyPlaying.setVisibility(View.VISIBLE);
        } else {
            holder.currentlyPlaying.setVisibility(View.GONE);
        }
    }

    public void setIndicator(long songId) {
        currSongId = songId;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class SongsViewHolder extends RecyclerView.ViewHolder {

        TextView songsText;
        TextView songsArtist;
        ImageView songsArt;
        ImageView currentlyPlaying;

        public SongsViewHolder(@NonNull View itemView) {
            super(itemView);
            songsText = itemView.findViewById(R.id.songTitleText);
            songsArtist = itemView.findViewById(R.id.songArtistText);
            songsArt = itemView.findViewById(R.id.songArtImg);
            currentlyPlaying = itemView.findViewById(R.id.curretlyPlaying);
        }
    }
}
