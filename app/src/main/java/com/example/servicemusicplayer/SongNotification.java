package com.example.servicemusicplayer;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class SongNotification extends Application {
    public static final String CHANNEL_ID = "Song Notification Channel";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
    }

    private void createNotificationChannel() {

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel songServiceChannel = new NotificationChannel(CHANNEL_ID, "MusicPlayer Notification", NotificationManager.IMPORTANCE_DEFAULT);

            getSystemService(NotificationManager.class).createNotificationChannel(songServiceChannel);
        }
    }
}
