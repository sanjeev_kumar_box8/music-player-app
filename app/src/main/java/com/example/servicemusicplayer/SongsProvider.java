package com.example.servicemusicplayer;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SongsProvider {

    public ArrayList<SongsDetails> findSongs(Context context) {

        ArrayList<SongsDetails> songsList = new ArrayList<>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{MediaStore.Audio.AudioColumns.DATA, MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.ALBUM, MediaStore.Audio.ArtistColumns.ARTIST,};
        Cursor cursor = context.getContentResolver().query(uri, projection, MediaStore.Audio.Media.DATA + " like ? ", new String[]{"%utm"}, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {

                String path = cursor.getString(0);
                String name = cursor.getString(1);
                String album = cursor.getString(2);
                String artist = cursor.getString(3);

//                songsList.add(new SongsDetails(name, path, album, artist));

                Log.i("songsListProvider", name + " " + path + " " + album + " " + artist);
            }
        } else {
            Log.i("songsListProvider", "Provider method failed");
        }

        return songsList;
    }
}
